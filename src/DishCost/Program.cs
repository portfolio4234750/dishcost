﻿using DishCost.IO;
using DishCost.Products;
using DishCost.Recipe;

namespace DishCost;

internal class Program {
    static void Main() {
        var flag = false;
        var cook = new Cook();
        do {
            try {
                var boughtProducts = Shopping.DoShopping();
                var recipe = cook.GetRecipe(boughtProducts);
                flag = true;
                var cost = Dish.GetCost(boughtProducts, recipe.Item1, recipe.Item2);
                Console.WriteLine("Стоимость порции блюда равна {0:0.00}.", cost);
            }
            catch (Exception e) {
                if (e is NotFoundProductException) continue;
                else {
                    IOAdapter.PrintError(e.Message);
                    break;
                }
            }
        } while (!flag);
    }
}