﻿namespace DishCost.Products;

/// <summary>Запись, описывающая продукт.</summary>
public record Product
{
    /// <summary>Название продукта.</summary>
    public string Name { get; init; } = "";

    /// <summary>Объем продукта в граммах/миллилитрах/штуках.</summary>
    public double Volume { get; init; } = 0;

    /// <summary>Стоимость продукта.</summary>
    public int Cost { get; init; } = 0;
}