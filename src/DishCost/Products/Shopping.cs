﻿using DishCost.IO;

namespace DishCost.Products;

/// <summary>Класс обработки покупок и хранения купленных продуктов.</summary>
public class Shopping {
    #region Constants
    
    private const string AskActionMessage =
        "Возможные действия:\n" +
        "1. Добавить продукт в покупки\n" +
        "2. Завершить покупку\n" +
        "3. Выход\n" +
        "Выберите действие (для этого введите номер действия): ";
    private const string ErrorActionMessage = "Такого действия нет! Попробуйте еще раз.";
    private const string ErrorIncorrectEnterMessage = "Вы ввели некорректное значение, попробуйте еще раз.";
    private const string AskProductNameMessage = "Введите название продукта: ";
    private const string AskProductVolumeMessage = "Введите объем (г/мл/штуки) продукта: ";
    private const string AskProductCostMessage = "Введите стоимость продукта: ";

    #endregion

    #region PublicMethods

    /// <summary>Метод для ввода информации о купленных продуктах и добавления их в список купленных.</summary>
    /// <param name="p">Список уже купленных продуктов.</param>
    public static List<Product> DoShopping(List<Product>? p = null) {
        var products = p ?? new List<Product>();
        do {
            var action = IOAdapter.GetIntValueFromConsole(AskActionMessage, ErrorActionMessage);
            if (action == 2) {
                if (products.Count == 0) Environment.Exit(0);
                else break;
            }
            else if (action == 3) Environment.Exit(0);
            else {
                var product = new Product {
                    Name = IOAdapter.GetStringValueFromConsole(AskProductNameMessage, ErrorIncorrectEnterMessage),
                    Volume = IOAdapter.GetDoubleValueFromConsole(AskProductVolumeMessage, ErrorIncorrectEnterMessage),
                    Cost = IOAdapter.GetIntValueFromConsole(AskProductCostMessage, ErrorIncorrectEnterMessage)
                };
                products.Add(product);
            }
        } while (true);

        return products;
    }

    #endregion
}