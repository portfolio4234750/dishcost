﻿namespace DishCost.IO;

/// <summary>Класс взаимодействия с пользователем (ввода-вывода).</summary>
public static class IOAdapter {
    /// <summary>
    /// Метод для вывода какой-либо ошибки пользователю.
    /// </summary>
    /// <param name="errorMessage">Текст ошибки.</param>
    public static void PrintError(string errorMessage) {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(errorMessage);
        Console.ResetColor();
    }

    /// <summary>
    /// Метод для запроса у пользователя какого-либо строкового значения.
    /// </summary>
    /// <param name="askMessage">Текст вопроса, на который пользователь должен ответить текстом.</param>
    /// <param name="errorMessage">Текст ошибки, которая будет выведена при неправильном вводе.</param>
    public static string GetStringValueFromConsole(string askMessage, string errorMessage) {
        var status = true;
        var value = "";
        while (status) {
            Console.Write(askMessage);
            value = Console.ReadLine()?.Trim();
            status = string.IsNullOrEmpty(value);
            if (status) {
                PrintError(errorMessage);
            }
        }
        return value!;
    }

    /// <summary>
    /// Метод для запроса у пользователя какого-либо целого положительного числа.
    /// </summary>
    /// <param name="askMessage">Текст вопроса, на который пользователь должен ответить числом.</param>
    /// <param name="errorMessage">Текст ошибки, которая будет выведена при неправильном вводе.</param>
    public static int GetIntValueFromConsole(string askMessage, string errorMessage) {
        var number = GetDoubleValueFromConsole(askMessage, errorMessage);
        return Convert.ToInt32(number);
    }

    /// <summary>
    /// Метод для запроса у пользователя какого-либо дробного положительного числа.
    /// </summary>
    /// <param name="askMessage">Текст вопроса, на который пользователь должен ответить числом.</param>
    /// <param name="errorMessage">Текст ошибки, которая будет выведена при неправильном вводе.</param>
    public static double GetDoubleValueFromConsole(string askMessage, string errorMessage) {
        var status = false;
        var value = -1d;
        while (!status) {
            Console.Write(askMessage);
            status = double.TryParse(Console.ReadLine()?.Trim(), out value);
            if (!status || value <= 0) {
                PrintError(errorMessage);
            }
        }
        return value;
    }
}