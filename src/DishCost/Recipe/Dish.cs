﻿using DishCost.Products;

namespace DishCost.Recipe;

/// <summary>Класс блюда из купленных продуктов и рецепта.</summary>
public static class Dish {
    /// <summary>Метод для расчета и вывода стоимости одной порции блюда.</summary>
    /// <param name="boughtProducts">Список купленных продуктов.</param>
    /// <param name="needProducts">Список продуктов, необходимых для рецепта.</param>
    /// <param name="countPortions">Количество порций, получающихся по рецепту.</param>
    public static double GetCost(List<Product> boughtProducts, List<Product> needProducts, int countPortions) {
        double cost = 0;

        foreach (var product in needProducts) {
            var boughtProduct = boughtProducts.First(p => p.Name == product.Name);
            cost += boughtProduct.Cost / boughtProduct.Volume * product.Volume;
        }

        cost /= countPortions;

        return Math.Round(cost, 2, MidpointRounding.ToPositiveInfinity);
    }
}