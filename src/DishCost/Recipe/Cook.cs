﻿using DishCost.IO;
using DishCost.Products;

namespace DishCost.Recipe;

/// <summary>Класс обработки необходимых продуктов для рецепта.</summary>
public class Cook {
    #region Constants

    private const string AskActionMessage = "Для дальнейшего расчета стоимости блюда необходимо добавить рецепт\n" +
                                            "Возможные действия:\n" +
                                            "1. Добавить ингридиент в рецепт\n" +
                                            "2. Закончить ввод рецепта\n" +
                                            "3. Выход\n" +
                                            "Выберите действие (для этого введите номер действия): ";
    private const string AskUnknownProductMessage = "Такого продукта нет в списке покупок!\n" +
                                                    "Возможные действия:\n" +
                                                    "1. Добавить продукт в список покупок\n" +
                                                    "2. Выход\n" +
                                                    "Выберите действие (для этого введите номер действия): ";
    private const string AskCountPortionsMessage = "Введите количество порций, которое получается по данному рецепту: ";
    private const string ErrorActionMessage = "Такого действия нет! Попробуйте еще раз.";
    private const string ErrorIncorrectEnterMessage = "Вы ввели некорректное значение, попробуйте еще раз.";
    private const string ErrorIncorrectVolumeMessage = "Для рецепта требуется больше продукта, чем Вы купили! " +
                                                       "Дальнейшая работа невозможна.";
    private const string AskProductNameMessage = "Введите название продукта: ";
    private const string AskProductVolumeMessage = "Введите необходимый объем (г/мл/штуки) продукта: ";

    #endregion

    #region Fields

    private List<Product> _products;

    #endregion

    #region .ctor

     /// <summary>.ctor</summary>
     public Cook() {
         _products = new List<Product>();
     }

    #endregion

    #region PublicMethods

    /// <summary>
    /// Метод для запроса информации о рецепте и добавления продуктов в список необходимых для рецепта.
    /// </summary>
    /// <param name="boughtProducts">Список купленных продуктов.</param>
    /// <param name="productsForRecipe">Список продуктов, необходимых для рецепта.</param>
    public (List<Product>, int) GetRecipe(List<Product> boughtProducts, List<Product>? productsForRecipe = null) {
        if (productsForRecipe != null) _products = productsForRecipe;
        do {
            var action = IOAdapter.GetIntValueFromConsole(AskActionMessage, ErrorActionMessage);
            if (action == 2) {
                if (_products.Count == 0) Environment.Exit(0);
                else break;
            }
            else if (action == 3) Environment.Exit(0);
            else {
                var name = IOAdapter.GetStringValueFromConsole(AskProductNameMessage, ErrorIncorrectEnterMessage);
                while (!boughtProducts.Exists(p => p.Name == name)) {
                    action = IOAdapter.GetIntValueFromConsole(AskUnknownProductMessage, ErrorActionMessage);
                    if (action == 2) Environment.Exit(0);
                    else throw new NotFoundProductException();
                }

                var volume = IOAdapter.GetDoubleValueFromConsole(AskProductVolumeMessage, ErrorIncorrectEnterMessage);
                if (boughtProducts.First(p => p.Name == name).Volume < volume) {
                    throw new LowVolumeException(ErrorIncorrectVolumeMessage);
                }

                var product = new Product() {
                    Name = name,
                    Volume = volume
                };
                _products.Add(product);
            }
        } while (true);

        var countPortions = IOAdapter.GetIntValueFromConsole(AskCountPortionsMessage, ErrorIncorrectEnterMessage);

        return (_products, countPortions);
    }

    #endregion
}

/// <summary>
/// Исключение, которое создается при возникновении ошибки отсутствия
/// необходимого продукта для рецепта в списке покупок.
/// </summary>
public class NotFoundProductException : NullReferenceException {

    public NotFoundProductException() : base() {}
}

public class LowVolumeException : Exception {
    public LowVolumeException(string message) : base(message) { }
}